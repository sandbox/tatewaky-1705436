README.txt
==========

A module that create a field on drupal entities in order to allow them to 
get reference for one or more terms of different vocabularies.


COMPATIBILITY NOTES
==================
- It is necesary and dependent the taxonomy module, and all its dependecnies just
like field and options.

AUTHOR/MAINTAINER
======================
- Jason Acuna <tatewaky> 
